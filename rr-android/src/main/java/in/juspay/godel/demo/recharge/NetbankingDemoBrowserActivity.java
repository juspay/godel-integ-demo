package in.juspay.godel.demo.recharge;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by gautam on 5/5/14.
 */
public class NetbankingDemoBrowserActivity extends Activity {
    private static final String LOG_TAG = NetbankingDemoBrowserActivity.class.getName();
    private static final String URL = "https://www.billdesk.com/pgidsk/wap/vodafonetup/vodafonechordiantconfirm.jsp";

    public static final String POST_DATA = "Pre_bonuscards1$hdnAccessfee=1.00&" +
            "Pre_bonuscards1$hdnBenefit=Talktime of Rs.7.9&" +
            "Pre_bonuscards1$hdnChannelId=RECHARGE&" +
            "Pre_bonuscards1$hdnCircleId=&" +
            "Pre_bonuscards1$hdnMRP=10.00&" +
            "Pre_bonuscards1$hdnMobileNumber=9620917775&" +
            "Pre_bonuscards1$hdnServiceTax=12.36&" +
            "Pre_bonuscards1$hdnTalktime=7.90&" +
            "Pre_bonuscards1$hdnaction=confirm_etopup&" +
            "Pre_bonuscards1$hdnairtime=0.00&" +
            "Pre_bonuscards1$hdnmsg=9620917775|10.00|0.00|1.00|12.36|Talktime of Rs.7.9|kar|RECHARGE|7.90|20140505170308|2144186221&" +
            "Pre_bonuscards1$txtDenom=10&" +
            "Pre_bonuscards1$txtmobileNo=9620917775&" +
            "__EVENTARGUMENT=&" +
            "__EVENTTARGET=&" +
            "__EVENTVALIDATION=/wEWCwLtyvX4DgLB39rQDgLIkufkCALl1YjvDALGn/GnAwKc6s+nCALI1fSCCQLI1bjxDgLI1czMBwLJgK/wCgKuwITEBoKlRHFTXgI5XV82o1qSAOneVyq1";


    private boolean isDebuggable(Context context) {
        return (0 != (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
    }

    public void notifyVersion(){
        if (isDebuggable(this)) {
            String godel_version = this.getString(in.juspay.godel.R.string.godel_version);
            String build_version = this.getString(in.juspay.godel.R.string.build_version);
            showMessageToast("Godel: " +  godel_version + " Build: " + build_version);
        }
    }

    private void showMessageToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        notifyVersion();
        Bundle intentBundle = new Bundle();
        intentBundle.putString("displayNote", "Recharge 9620917775 with RS 10");
        intentBundle.putString("customerId", "9620917775");
        intentBundle.putString("customerEmail", "gautam@juspay.in");
        intentBundle.putString("remarks", "Recharge 9620917775");
        intentBundle.putString("merchantId", "juspay_recharge_netbanking");
        intentBundle.putString("clientId", "juspay_recharge");
        intentBundle.putString("customerPhoneNumber", "9620917775");
        intentBundle.putString("transactionId", "netbanking_demo_transaction_id");
        intentBundle.putString("url", URL);
        intentBundle.putString("postData", POST_DATA);
        intentBundle.putBoolean("progressDialogEnabled", true);

        Intent browserIntent = new Intent();
        browserIntent.putExtras(intentBundle);
        browserIntent.setClass(NetbankingDemoBrowserActivity.this, JuspayBrowserActivity.class);
        startActivity(browserIntent);
        finish();
        super.onCreate(intentBundle);
    }
}
