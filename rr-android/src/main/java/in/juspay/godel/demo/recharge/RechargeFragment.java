package in.juspay.godel.demo.recharge;

import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import in.juspay.godel.demo.R;
import in.juspay.godel.ui.GodelFragment;
import in.juspay.godel.ui.NetworkTask;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RechargeFragment extends GodelFragment {

    private String selectedMobileNumber;
    private String selectedAmount;
    private String selectedCircle;
    private String selectedOperator;

    private final RechargeActivity rechargeActivity;

    public RechargeFragment(RechargeActivity rechargeActivity) {
        super();
        this.rechargeActivity = rechargeActivity;
    }

    private class CreateRechargeRequestTask extends NetworkTask<Object, Object ,RechargeRequest> {
        private final String mobileNumber;
        private final String amount;

        public CreateRechargeRequestTask(String mobileNumber, String amount) {
            super(rechargeActivity);
            this.mobileNumber = mobileNumber;
            this.amount = amount;
        }

        @Override
        protected RechargeRequest doInBackground(Object... params) {
            setMessage("Could not connect to the internet. Please retry.");
            RechargeRequest rechargeRequest = new RechargeRequest();
            rechargeRequest.setMobileNumber(mobileNumber);
            rechargeRequest.setOperator(mOperators.getSelectedItem().toString());
            rechargeRequest.setCircle(mCircles.getSelectedItem().toString());
            rechargeRequest.setAmount(amount);
            rechargeRequest.setOperatorCode(operatorCode);
            rechargeRequest.setCircleCode(circleCode);
            rechargeRequest.setDateCreated(new Date());
            return new RechargeService().createRechargeRequest(rechargeRequest);
        }

        @Override
        protected void onPostExecute(final RechargeRequest rechargeRequest) {

            Bundle args = new Bundle();
            args.putString("merchantId", "juspay_recharge");
            args.putString("orderId", rechargeRequest.getPaymentId());
            args.putString("amount", mAmount.getText().toString());
            args.putBoolean("noStoredCards", true);

            rechargeActivity.setRechargeRequest(rechargeRequest);
            rechargeActivity.showFragment(new PaymentFragment());
        }

        @Override
        protected void onAlertDismiss(DialogInterface dialog) {
            mRechargeButton.setText("Recharge");
            mRechargeButton.setEnabled(true);
        }
    }

    private AutoCompleteTextView mNumber;
    private EditText mAmount;
    private Button mRechargeButton;
    private Spinner mOperators;
    private Spinner mCircles;

    private String operatorCode;
    private String circleCode;

    private void setValidationState() {
        String number = mNumber.getText().toString();
        String amount = mAmount.getText().toString();

        if(number.length() == 10 && amount.length() > 0 && Integer.parseInt(amount) >= 10) {
            mRechargeButton.setEnabled(true);
        }
        else {
            mRechargeButton.setEnabled(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mRechargeButton.setText(R.string.label_recharge_button);
        mRechargeButton.setEnabled(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            selectedMobileNumber = (String) savedInstanceState.get("mobileNumber");
            selectedAmount = (String) savedInstanceState.get("amount");
            selectedOperator = (String) savedInstanceState.get("operator");
            selectedCircle = (String) savedInstanceState.get("circle");
        }

        // lets get the past recharged numbers
        List<RechargeRequest> rechargeRequestList = new ArrayList<RechargeRequest>();
        ArrayList<String> previousMobileNumbers = new ArrayList<String>();
        if(null != rechargeRequestList && rechargeRequestList.size() > 0){
            for (RechargeRequest account : rechargeRequestList) {
                previousMobileNumbers.add(account.getMobileNumber());
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_recharge_form, container, false);

        // initialize the elements
        mRechargeButton = (Button) rootView.findViewById(R.id.recharge_form_submit_btn);
        mNumber = (AutoCompleteTextView) rootView.findViewById(R.id.mobile_number);
        mAmount = (EditText) rootView.findViewById(R.id.recharge_form_amount_text);
        mOperators = (Spinner) rootView.findViewById(R.id.operators_spinner);
        mCircles = (Spinner) rootView.findViewById(R.id.circles_spinner);

        ArrayAdapter<String> numberList = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_dropdown_item_1line, previousMobileNumbers);
        mNumber.setAdapter(numberList);

        final List<String> operatorList = new ArrayList<String>(Constants.telecomOperatorMap.keySet());
        ArrayAdapter<String> operatorArrayAdapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, operatorList);
        operatorArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mOperators.setAdapter(operatorArrayAdapter);
        mOperators.setPromptId(R.string.operator_prompt);

        final List<String> circleList = new ArrayList<String>(Constants.telecomCircleMap.keySet());
        ArrayAdapter<String> circleArrayAdapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, circleList);
        circleArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCircles.setAdapter(circleArrayAdapter);
        mCircles.setPromptId(R.string.circle_prompt);

        operatorCode = Constants.telecomOperatorMap.get(operatorList.get(0));
        circleCode = Constants.telecomCircleMap.get(circleList.get(0));

        mOperators.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(Constants.TAG, "Operator chosen: " + operatorList.get(i));
                operatorCode = Constants.telecomOperatorMap.get(operatorList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mCircles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(Constants.TAG, "Circle chosen: " + circleList.get(i));
                circleCode = Constants.telecomCircleMap.get(circleList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //testing ToDo: del this
        boolean isDebuggable =  ( 0 != ( getActivity().getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE ) );
        if(isDebuggable && selectedMobileNumber == null) {
            mNumber.setText("9962881912");
            mAmount.setText("10");
        }
        else {
            if(selectedMobileNumber != null) {
                mNumber.setText(selectedMobileNumber);
                mAmount.setText(selectedAmount);
                for(int i=0; i<operatorList.size(); i++) {
                    if(operatorList.get(i).equals(selectedOperator)) {
                        mOperators.setSelection(i);
                        operatorCode = Constants.telecomOperatorMap.get(selectedOperator);
                        break;
                    }
                }
                for(int i=0; i<circleList.size(); i++) {
                    if(circleList.get(i).equals(selectedCircle)) {
                        mCircles.setSelection(i);
                        circleCode = Constants.telecomCircleMap.get(selectedCircle);
                        break;
                    }
                }
            }
        }

        TextWatcher rechargeFormWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override
            public void afterTextChanged(Editable editable) {
                setValidationState();
            }
        };

        mNumber.addTextChangedListener(rechargeFormWatcher);
        mAmount.addTextChangedListener(rechargeFormWatcher);

        mNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence.length() == 10) {
                    // We can do this once we implement auto-selection of operator and circle
                    // mAmount.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mRechargeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRechargeButton.setText("Please wait ...");
                mRechargeButton.setEnabled(false);
                new CreateRechargeRequestTask(mNumber.getText().toString(), mAmount.getText().toString()).execute();

                Log.d("RR", "inside recharge button");
            }
        });
        return rootView;
    }
}
