package in.juspay.godel.demo.recharge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import in.juspay.godel.browser.JuspayWebChromeClient;
import in.juspay.godel.browser.JuspayWebViewClient;
import in.juspay.godel.core.PaymentResponse;
import in.juspay.godel.demo.R;
import in.juspay.godel.ui.JuspayBrowserFragment;
import in.juspay.godel.ui.JuspayWebView;

public class JuspayBrowserActivity extends FragmentActivity {
    private static final String LOG_TAG = JuspayBrowserActivity.class.getName();
    private JuspayBrowserFragment browserFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juspay_browser);

        browserFragment = (JuspayBrowserFragment) getSupportFragmentManager().findFragmentById(R.id.juspay_browser_fragment);
        browserFragment.getWebView().getSettings().setJavaScriptEnabled(true);
        browserFragment.startPaymentWithArguments(getIntent().getExtras());
    }

    @Override
    public void onBackPressed() {
        browserFragment.juspayBackPressedHandler();
        super.onBackPressed();
    }

    public JuspayWebChromeClient newWebChromeClient(JuspayBrowserFragment fragment) {
        return new JuspayWebChromeClient(fragment);
    }

    public JuspayWebViewClient newWebViewClient(JuspayWebView webView, JuspayBrowserFragment fragment) {
        return new JuspayWebViewClient(webView, fragment);
    }

    protected void handlePaymentResponse(PaymentResponse paymentResponse) {
        Intent intent = new Intent();
        intent.putExtra("orderId", paymentResponse.getOrderId());
        intent.putExtra("merchantId", paymentResponse.getMerchantId());
        intent.putExtra("status", paymentResponse.getStatus());
        intent.putExtra("paymentResponse", paymentResponse);

        JuspayBrowserActivity.this.setResult(Activity.RESULT_OK, intent);
        JuspayBrowserActivity.this.finish();
    }

    public JuspayBrowserFragment getBrowserFragment() {
        return browserFragment;
    }

    public static class BrowserFragment extends JuspayBrowserFragment {
        private JuspayBrowserActivity activity;

        @Override
        public void onReceivedError() {
            activity.finish();
        }

        @Override
        public void handlePaymentResponse(PaymentResponse paymentResponse) {
            activity.handlePaymentResponse(paymentResponse);
        }

        @Override
        protected JuspayWebChromeClient newWebChromeClient() {
            return activity.newWebChromeClient(this);
        }

        @Override
        protected JuspayWebViewClient newWebViewClient(JuspayWebView juspayWebView) {
            return activity.newWebViewClient(juspayWebView, this);
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            this.activity = (JuspayBrowserActivity) activity;
        }
    }
}
