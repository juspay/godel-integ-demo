package in.juspay.godel.demo.model;

public class PaymentAuthenticationResponse {
    String authenticationUrl;

    public String getAuthenticationUrl() {
        return authenticationUrl;
    }

    public void setAuthenticationUrl(String authenticationUrl) {
        this.authenticationUrl = authenticationUrl;
    }
}
