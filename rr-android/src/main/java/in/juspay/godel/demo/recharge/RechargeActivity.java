package in.juspay.godel.demo.recharge;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import in.juspay.godel.browser.JuspayWebChromeClient;
import in.juspay.godel.browser.JuspayWebViewClient;
import in.juspay.godel.core.Card.CardType;
import in.juspay.godel.core.PaymentResponse;
import in.juspay.godel.demo.R;
import in.juspay.godel.demo.custom.CustomWebViewClient;
import in.juspay.godel.demo.model.PaymentAuthenticationResponse;
import in.juspay.godel.ui.JuspayBrowserFragment;
import in.juspay.godel.ui.JuspayWebView;

public class RechargeActivity extends FragmentActivity {

    private static final String LOG_TAG = RechargeActivity.class.getName();

    private RechargeRequest rechargeRequest;
    private PaymentAuthenticationResponse paymentAuthResponse;
    private JuspayBrowserFragment browserFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_screen);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new RechargeFragment(this))
                .commit();
    }

    public void showFragment(Fragment fragment) {
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if(browserFragment!=null){
            browserFragment.juspayBackPressedHandler();
        }
        super.onBackPressed();
    }

    /**
     * Kick off the authentication process for the recharge request.
     */
    public void startAuthentication() {
        // we now have the authentication url
        browserFragment = new JuspayBrowserFragment() {
            /**
             * Returns custom webview client which will know when to stop the webview and get the control
             * back to the application
             */
            @Override
            protected JuspayWebViewClient newWebViewClient(JuspayWebView webView) {
                CustomWebViewClient customWebViewClient = new CustomWebViewClient(webView, this, RechargeActivity.this);
                return customWebViewClient;
            }

            @Override
            protected JuspayWebChromeClient newWebChromeClient() {
                return new JuspayWebChromeClient(this);
            }
        };

        // setup the initial parameter(s) for the browser fragment
        Bundle args = new Bundle();
        args.putString("url", paymentAuthResponse.getAuthenticationUrl());
        args.putString("merchantId", "juspay_recharge");
        args.putString("transactionId", rechargeRequest.getPaymentId());
        args.putString("clientId", "juspay_recharge_android");

        args.putString("customerId", rechargeRequest.getMobileNumber());
        args.putString("displayNote", String.format("Recharging %s for Rs. %s", rechargeRequest.getMobileNumber(),
                rechargeRequest.getAmount()));
        args.putString("remarks", String.format("Recharging %s for Rs. %s", rechargeRequest.getMobileNumber(),
                rechargeRequest.getAmount()));
        args.putString("amount", rechargeRequest.getAmount());
        args.putString("customerEmail", "");
        args.putString("customerPhoneNumber", rechargeRequest.getMobileNumber());
        args.putString("udf_operator", rechargeRequest.getOperator());
        args.putString("udf_circle", rechargeRequest.getCircle());
        args.putString("udf_type", "prepaid");
        args.putBoolean("progressDialogEnabled", true);
        args.putSerializable("card_type", CardType.CREDIT_CARD);
        browserFragment.setArguments(args);

        // attach it to the Fragment Manager
        showFragment(browserFragment);
    }

    /**
     * Payment is completed for the existing recharge request. Lets check the status and initiate the recharge
     * if required.
     */
    public void handlePaymentResponse(String url) {
        new AsyncTask<RechargeRequest, String, PaymentResponse>(){
            @Override
            protected PaymentResponse doInBackground(RechargeRequest... rechargeRequests) {
                return new RechargeService().checkPaymentStatus(rechargeRequest);
            }

            @Override
            protected void onPostExecute(PaymentResponse paymentResponse) {
                Intent intent = new Intent(RechargeActivity.this, PaymentResponseActivity.class);
                intent.putExtra("paymentResponse", paymentResponse);
                intent.putExtra("rechargeRequest", rechargeRequest);
                startActivity(intent);
            }
        }.execute(rechargeRequest, null, null);
    }

    public RechargeRequest getRechargeRequest() {
        return rechargeRequest;
    }

    public void setRechargeRequest(RechargeRequest rechargeRequest) {
        this.rechargeRequest = rechargeRequest;
    }

    public void setPaymentAuthenticationResponse(PaymentAuthenticationResponse authResponse) {
        this.paymentAuthResponse = authResponse;
    }
}
