package in.juspay.godel.demo.recharge;

import java.io.Serializable;
import java.util.Date;

public class RechargeRequest implements Serializable {

    String uniqueReferenceNumber;
    String mobileNumber;
    String circleCode;
    String operatorCode;
    String circle;
    String operator;
    String amount;
    String email;
    String associateId;
    String rechargeStatus;
    Date dateCreated;

    String id;
    String paymentId;
    String paymentUrl;
    String acsUrl;
    String md;
    String termUrl;
    String pareq;

    public RechargeRequest(String mobilenum, String amount, String area, String operator)
    {
        setOperator(operator);
        setMobileNumber(mobilenum);
        setAmount(amount);
        setCircle(area);
    }


    public RechargeRequest()
    {}
    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getAcsUrl() {

        return acsUrl;
    }

    public void setAcsUrl(String acsUrl) {
        this.acsUrl = acsUrl;
    }

    public String getMd() {
        return md;
    }

    public void setMd(String md) {
        this.md = md;
    }

    public String getTermUrl() {
        return termUrl;
    }

    public void setTermUrl(String termUrl) {
        this.termUrl = termUrl;
    }

    public String getPareq() {
        return pareq;
    }

    public void setPareq(String pareq) {
        this.pareq = pareq;
    }

    public String getRechargeStatus() {
        return rechargeStatus;
    }

    public void setRechargeStatus(String rechargeStatus) {
        this.rechargeStatus = rechargeStatus;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCircleCode() {
        return circleCode;
    }

    public void setCircleCode(String circleCode) {
        this.circleCode = circleCode;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUniqueReferenceNumber() {
        return uniqueReferenceNumber;
    }

    public void setUniqueReferenceNumber(String uniqueReferenceNumber) {
        this.uniqueReferenceNumber = uniqueReferenceNumber;
    }

    public String getAssociateId() {
        return associateId;
    }

    public void setAssociateId(String associateId) {
        this.associateId = associateId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public String toString() {
        return "RechargeRequest{" +
                "uniqueReferenceNumber='" + uniqueReferenceNumber + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", circleCode='" + circleCode + '\'' +
                ", operatorCode='" + operatorCode + '\'' +
                ", circle='" + circle + '\'' +
                ", operator='" + operator + '\'' +
                ", amount='" + amount + '\'' +
                ", email='" + email + '\'' +
                ", associateId='" + associateId + '\'' +
                ", rechargeStatus='" + rechargeStatus + '\'' +
                ", id='" + id + '\'' +
                ", paymentId='" + paymentId + '\'' +
                ", paymentUrl='" + paymentUrl + '\'' +
                ", acsUrl='" + acsUrl + '\'' +
                ", md='" + md + '\'' +
                ", termUrl='" + termUrl + '\'' +
                ", pareq='" + pareq + '\'' +
                '}';
    }
}
