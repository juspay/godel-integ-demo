package in.juspay.godel.demo.recharge;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import in.juspay.godel.demo.R;
import in.juspay.godel.demo.model.Card;
import in.juspay.godel.demo.model.PaymentAuthenticationResponse;
import in.juspay.godel.demo.model.PaymentRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class PaymentFragment extends Fragment {

    EditText mCardNumber;
    EditText mCardExpiryMonth;
    EditText mCardExpiryYear;
    EditText mCardCVV;
    Button mPaymentButton;
    TextView mPaymentFormErrorText;
    Card card;

    public PaymentFragment() {

    }

    private void setValidationState() {

        String cardNumber = mCardNumber.getText().toString();
        String cardExpiryMonth = mCardExpiryMonth.getText().toString();
        String cardExpiryYear = mCardExpiryYear.getText().toString();
        String cardCVV = mCardCVV.getText().toString();

        if(cardNumber.length() == 16 && cardExpiryMonth.length() > 0
                && cardExpiryYear.length() == 2 && cardCVV.length() == 3) {
            mPaymentButton.setEnabled(true);
        }
        else {
            mPaymentButton.setEnabled(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPaymentButton.setText(R.string.label_payment_button);
        mPaymentButton.setEnabled(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.payment_fragment, container, false);
        // initialize view elements
        mCardNumber = (EditText) rootView.findViewById(R.id.payment_form_card_number);
        mCardExpiryMonth = (EditText) rootView.findViewById(R.id.payment_form_card_expiry_month);
        mCardExpiryYear = (EditText) rootView.findViewById(R.id.payment_form_card_expiry_year);
        mCardCVV = (EditText) rootView.findViewById(R.id.payment_form_card_cvv);
        mPaymentButton = (Button) rootView.findViewById(R.id.payment_form_submit_button);
        mPaymentFormErrorText = (TextView) rootView.findViewById(R.id.textView);

        // disable the button to start with
        mPaymentButton.setEnabled(false);

        TextWatcher paymentFormWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override
            public void afterTextChanged(Editable editable) {
                setValidationState();
            }
        };

        // set listeners on all the text field
        List<EditText> textElements = Arrays.asList(new EditText[]{mCardNumber, mCardExpiryMonth, mCardExpiryYear, mCardCVV});
        for(EditText element: textElements) {
            element.addTextChangedListener(paymentFormWatcher);
        }

        mCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                String text = charSequence.toString();
                if(text.length() == 16) {
                    mCardExpiryMonth.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        mCardExpiryMonth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                String text = charSequence.toString();
                if(text.length() == 2 || (text.length() == 1 && Integer.parseInt(text) > 1)) {
                    mCardExpiryYear.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        mCardExpiryYear.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                String text = charSequence.toString();
                if(text.length() == 2) {
                    mCardCVV.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        mPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPaymentButton.setText("Please wait ...");
                mPaymentButton.setEnabled(false);

                card = new Card(mCardNumber.getText().toString(),
                        Integer.parseInt(mCardExpiryMonth.getText().toString()),
                        Integer.parseInt(mCardExpiryYear.getText().toString()));

                card.setCVV(mCardCVV.getText().toString());
                final RechargeActivity rechargeActivity = (RechargeActivity) getActivity();
                final RechargeRequest rechargeRequest = rechargeActivity.getRechargeRequest();
                final PaymentRequest paymentRequest = new PaymentRequest();
                paymentRequest.setCard(card);
                paymentRequest.setOrderId(rechargeRequest.getPaymentId());
                paymentRequest.setMerchantId("juspay_recharge");
                new AsyncTask<Object, Object , PaymentAuthenticationResponse>(){
                    @Override
                    protected PaymentAuthenticationResponse doInBackground(Object... objects) {
                        PaymentAuthenticationResponse authResponse = new RechargeService().startPayment(paymentRequest);
                        return authResponse;
                    }

                    @Override
                    protected void onPostExecute(PaymentAuthenticationResponse paymentAuthenticationResponse) {
                        super.onPostExecute(paymentAuthenticationResponse);
                        rechargeActivity.setPaymentAuthenticationResponse(paymentAuthenticationResponse);
                        rechargeActivity.startAuthentication();
                    }
                }.execute(null, null, null);
            }
        });
        return rootView;
    }

    public Card getCard() {
        return card;
    }

    public String getCardJson() {
        JSONObject cardJson = new JSONObject();
        try {
            cardJson.put("cardNumber", card.getCardNumber());
            cardJson.put("cardExpiryMonth", card.getExpiryMonth());
            cardJson.put("cardExpiryYear", card.getExpiryYear());
            cardJson.put("cardSecurityCode", card.getCVV());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cardJson.toString();
    }

    public void showPaymentErrorMessage() {
        mPaymentButton.setText("Pay");
        mPaymentButton.setEnabled(true);
        mPaymentFormErrorText.setText("Invalid Entry");
        mPaymentFormErrorText.setVisibility(View.VISIBLE);
    }
}
