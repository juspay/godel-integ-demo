package in.juspay.godel.demo.model;

import in.juspay.godel.core.CardBrand;

import java.io.Serializable;

public class Card implements Serializable {
    private CardBrand cardBrand;
    private final String cardNumber;
    private final int expiryMonth;
    private final int expiryYear;
    private String nameOnCard;
    private String cvv;

    public Card(String cardNumber, int expiryMonth, int expiryYear) {
        this.cardNumber = cardNumber;
        this.expiryMonth = expiryMonth;
        this.expiryYear = expiryYear;
        this.cardBrand = CardBrand.UNKNOWN;
        this.nameOnCard = "";
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public int getExpiryYear() {
        return expiryYear;
    }

    public int getExpiryMonth() {
        return expiryMonth;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public CardBrand getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        try {
            setCardBrand(CardBrand.valueOf(cardBrand.toUpperCase()));
        } catch (IllegalArgumentException e) {
            setCardBrand(CardBrand.UNKNOWN);
        }
    }

    public void setCardBrand(CardBrand cardBrand) {
        this.cardBrand = cardBrand;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getCVV() {
        return cvv;
    }

    public void setCVV(String cvv) {
        this.cvv = cvv;
    }
}
