package in.juspay.godel.demo.recharge;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by ram on 6/3/14.
 */
public class Constants {
    public static final String TAG = "RM";

    public static final HashMap<String, String> telecomOperatorMap = new LinkedHashMap<String, String>(){{
        put("Airtel", "1");
        put("Vodafone", "2");
        put("BSNL", "3");
        put("Idea", "8");
        put("Tata Docomo", "11");
        put("Reliance CDMA", "4");
        put("Reliance GSM", "5");
        put("Aircel", "6");
        put("MTS", "13");
        put("Loop Mobile", "10");
        put("S Tel", "15");
        put("Tata Indicom", "9");
        put("Uninor", "16");
        put("Videocon", "17");
        put("Virgin GSM", "14");
        put("Virgin CDMA", "12");
    }};

    public static final HashMap<String, String> telecomCircleMap = new LinkedHashMap<String, String>(){{
        put("Chennai", "4");
        put("Delhi NCR", "5");
        put("Mumbai", "15");
        put("Kolkata", "12");
        put("Andhra Pradesh", "1");
        put("Arunachal Pradesh", "16");
        put("Assam", "2");
        put("Bihar", "3");
        put("Chattisgarh", "14");
        put("Goa", "13");
        put("Gujarat", "6");
        put("Haryana", "7");
        put("Himachal Pradesh", "8");
        put("Jammu & Kashmir", "9");
        put("Jharkhand", "3");
        put("Karnataka", "10");
        put("Kerala", "11");
        put("Madhya Pradesh", "14");
        put("Maharashtra", "13");
        put("Manipur", "16");
        put("Meghalaya", "16");
        put("Mizoram", "16");
        put("Nagaland", "16");
        put("Orissa", "17");
        put("Punjab", "18");
        put("Rajasthan", "19");
        put("Sikkim", "23");
        put("Tamil Nadu", "20");
        put("Tripura", "16");
        put("Uttar Pradesh (East)", "21");
        put("Uttar Pradesh (West)", "22");
        put("Uttarakhand", "22");
        put("West Bengal", "23");
    }};
}
